package com.gamesys.test.userservice.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.gamesys.test.userservice.service.UserService;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)

public class UserControllerTest {
	
	@Autowired
    private MockMvc mvc;
	
	@MockBean
    private UserService service;
	
	@Test
	public void givenInvalidPassword_whenPostUser_thenReturn400()
	  throws Exception {
	    
	    String json = "{\"username\":\"Arruda\", \"dateOfBirth\": \"2019-01-20\", \"ssn\": \"123456789\", \"password\": \"abcd1234\"}";
	    
	    mvc.perform(
	            post("/users")
	            .contentType(MediaType.APPLICATION_JSON)
	            .accept(MediaType.APPLICATION_JSON)
	            .content(json))
	    		.andDo(print())
	            .andExpect(status().isBadRequest());
	}
	
	@Test
	public void givenInvalidDateOfBirth_whenPostUser_thenReturn400()
	  throws Exception {
	    
	    String json = "{\"username\":\"Arruda\", \"dateOfBirth\": \"201-a01\", \"ssn\": \"123456789\", \"password\": \"aBcd1234\"}";
	    
	    mvc.perform(
	            post("/users")
	            .contentType(MediaType.APPLICATION_JSON)
	            .accept(MediaType.APPLICATION_JSON)
	            .content(json))
	    		.andDo(print())
	            .andExpect(status().isBadRequest());
	}
	
	@Test
	public void givenInvalidUsername_whenPostUser_thenReturn400()
	  throws Exception {
	    
	    String json = "{\"username\":\"Arruda-*\", \"dateOfBirth\": \"2019-01-01\", \"ssn\": \"123456789\", \"password\": \"aBcd1234\"}";
	    
	    mvc.perform(
	            post("/users")
	            .contentType(MediaType.APPLICATION_JSON)
	            .accept(MediaType.APPLICATION_JSON)
	            .content(json))
	    		.andDo(print())
	            .andExpect(status().isBadRequest());
	}
	
	@Test
	public void givenInvalidSsn_whenPostUser_thenReturn400()
	  throws Exception {
	    
	    String json = "{\"username\":\"Arruda\", \"dateOfBirth\": \"2019-01-01\", \"ssn\": \"12356789\", \"password\": \"aBcd1234\"}";
	    
	    mvc.perform(
	            post("/users")
	            .contentType(MediaType.APPLICATION_JSON)
	            .accept(MediaType.APPLICATION_JSON)
	            .content(json))
	    		.andDo(print())
	            .andExpect(status().isBadRequest());
	}

}
