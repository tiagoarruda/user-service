package com.gamesys.test.userservice.customValidator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CustomPasswordValidatorTest {
	
	private CustomPasswordValidator validator = new CustomPasswordValidator();
	
	@Test
	public void givenValidPassword_whenValidatingPassword_returnTrue() {
		String validPassword = "Ab12";
		
		assertTrue(validator.isValid(validPassword, null));
	}
	
	@Test
	public void givenInvalidPasswordMissingUpperCase_whenValidatingPassword_returnFalse() {
		String validPassword = "ab12";
		
		assertFalse(validator.isValid(validPassword, null));
	}

}
