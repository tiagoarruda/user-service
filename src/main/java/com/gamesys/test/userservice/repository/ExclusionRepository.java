package com.gamesys.test.userservice.repository;

import java.util.Date;

import org.springframework.data.repository.CrudRepository;

import com.gamesys.test.userservice.entity.Exclusion;

public interface ExclusionRepository extends CrudRepository<Exclusion, String> {
	
	Exclusion getByDateOfBirthAndSsn(Date dateOfBirth, String ssn);
	
	String deleteByDateOfBirthAndSsn(Date dateOfBirth, String ssn);

}
