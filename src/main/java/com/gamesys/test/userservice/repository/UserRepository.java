package com.gamesys.test.userservice.repository;

import org.springframework.data.repository.CrudRepository;

import com.gamesys.test.userservice.entity.User;

public interface UserRepository extends CrudRepository<User, String> {

}
