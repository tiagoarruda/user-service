package com.gamesys.test.userservice.controller;

import java.util.logging.Logger;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.gamesys.test.userservice.entity.User;
import com.gamesys.test.userservice.service.UserService;

@RestController
public class UserController {
	
	private final static Logger logger = Logger.getLogger(UserController.class.getName());
	
	@Autowired
	private UserService service;
	
	@PostMapping(value="/users", produces = "application/json", consumes = "application/json")
	@ResponseStatus(value = HttpStatus.CREATED)
	public User createUser(@Valid @RequestBody User user) {
		
		logger.info("Received POST request to create a new user.");
		
		return service.createUser(user);
	}
	
	@GetMapping(value="/users/{username}", produces = "application/json",consumes = "application/json")
	public User getUser(@PathVariable("username") String username) {
		
		logger.info("Received GET request to get user by username.");
		
		return service.getUser(username);
	}
	
	@DeleteMapping(value="/users/{username}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void deleteUser(@PathVariable("username") String username) {
		
		logger.info("Received DELETE request to delete user by username.");
		
		service.deleteUser(username);
	}

}
