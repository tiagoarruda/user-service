package com.gamesys.test.userservice.controller;

import java.util.logging.Logger;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.gamesys.test.userservice.entity.Exclusion;
import com.gamesys.test.userservice.service.ExclusionService;

@RestController
public class ExclusionController {
	
	private final static Logger logger = Logger.getLogger(ExclusionController.class.getName());
	
	@Autowired
	private ExclusionService service;
	
	@PostMapping(value="/exclusions", produces = "application/json", consumes = "application/json")

	@ResponseStatus(value = HttpStatus.CREATED)
	public Exclusion createExclusion(@Valid @RequestBody Exclusion exclusion) {
		
		logger.info("Received POST request to create a new exclusion.");
		
		return service.createExclusion(exclusion);
	}
	
	@DeleteMapping(value="/exclusions")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void deleteExclusion(@NotNull @RequestParam("dateOfBirth") String dateOfBirth, @NotNull @RequestParam("ssn") String ssn) {
		
		logger.info("Received DELETE request to delete exclusion by date of birth and ssn.");
		
		service.delete(dateOfBirth, ssn);
	}
	
}
