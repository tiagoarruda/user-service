package com.gamesys.test.userservice.service;

import java.util.Date;

import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gamesys.test.userservice.entity.Exclusion;
import com.gamesys.test.userservice.repository.ExclusionRepository;

@Service
public class ExclusionServiceImpl implements ExclusionService {
	
	@Autowired
	private ExclusionRepository repo;

	@Override
	public boolean validate(String dateOfBirth, String ssn) {
		return getExclusion(dateOfBirth, ssn) == null;
	}
	
	@Override
	public Exclusion getExclusion(String dateOfBirth, String ssn) {
		DateTimeFormatter formatter = ISODateTimeFormat.dateOptionalTimeParser();
		Date queryDate  = formatter.parseDateTime(dateOfBirth).toDate();
		
		return repo.getByDateOfBirthAndSsn(queryDate, ssn);
	}

	@Override
	public Exclusion createExclusion(Exclusion exclusion) {
		
		return repo.save(exclusion);
	}

	@Override
	public String delete(String dateOfBirth, String ssn) {
		DateTimeFormatter formatter = ISODateTimeFormat.dateOptionalTimeParser();
		Date queryDate  = formatter.parseDateTime(dateOfBirth).toDate();
		
		return repo.deleteByDateOfBirthAndSsn(queryDate, ssn);
	}

}
