package com.gamesys.test.userservice.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.gamesys.test.userservice.entity.User;
import com.gamesys.test.userservice.exception.ResourceNotFoundException;
import com.gamesys.test.userservice.exception.UserExclusionException;
import com.gamesys.test.userservice.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository repo;
	
	@Autowired
	private ExclusionService exclusionService;
	
	@Autowired
	private PasswordEncoder encoder;

	@Override
	public User createUser(User user) {
		if (!exclusionService.validate(user.getDateOfBirth(), user.getSsn())) 
			throw new UserExclusionException(user.getSsn());
		
		//Encode password
		user.setPassword(encoder.encode(user.getPassword()));

		return repo.save(user);
	}

	@Override
	public User getUser(String userName) {
		Optional<User> result = repo.findById(userName);
		if (!result.isPresent())
			throw new ResourceNotFoundException("User", "userName", userName);
		
		return result.get();
	}

	@Override
	public void deleteUser(String username) {
		repo.deleteById(username);
	}

}
