package com.gamesys.test.userservice.service;

import com.gamesys.test.userservice.entity.User;

public interface UserService {
	User createUser(User user);
	User getUser(String userName);
	void deleteUser(String username);
}
