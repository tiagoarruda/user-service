package com.gamesys.test.userservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class UserExclusionException extends RuntimeException {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8205116686261226354L;
	private String ssn;


    
    public UserExclusionException( String ssn) {
        super(String.format("SSN %s found at exclusion list", ssn));
        this.ssn = ssn;
    }
    public String getSSN() {
        return ssn;
    }

}
