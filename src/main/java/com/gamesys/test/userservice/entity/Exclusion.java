package com.gamesys.test.userservice.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

@Entity
public class Exclusion {

	@NotNull
	private Date dateOfBirth;
	
	@NotNull
	@Size(min=9, max=9, message="Social Security number (SSN) is a nine-digit number")
	@Id
	private String ssn;

	//date of birth
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	//tranforming iso 8601 to java date
	public void setDateOfBirth(String dateOfBirth) {
		DateTimeFormatter formatter = ISODateTimeFormat.dateOptionalTimeParser();
		this.dateOfBirth = formatter.parseDateTime(dateOfBirth).toDate();
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	//ssn
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	
	
}
