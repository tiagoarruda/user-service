package com.gamesys.test.userservice.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.gamesys.test.userservice.customValidator.NoSpaces;
import com.gamesys.test.userservice.customValidator.Password;

@Entity
public class User {
	@Column(unique=true)
	@NotNull
	@NoSpaces
	@Pattern(regexp = "^[\\p{Alnum}]{1,32}$", message="Username type only allows alphanumerical characters")
	@Id
	private String username;
	
	@NotNull
	@Password
	@JsonProperty(access = Access.WRITE_ONLY)
	private String password;
	
	@NotNull
	private Date dateOfBirth;
	
	@NotNull
	@Size(min=9, max=9, message="Social Security number (SSN) is a nine-digit number")
	private String ssn;
	
	//username
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	//password
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	//date of birth
	@JsonIgnore
	public Date getDateOfBirthDate() {
		return dateOfBirth;
	}
	public String getDateOfBirth() {
		DateTime date = new DateTime(this.dateOfBirth);
		
		return date.toString();
	}
	//tranforming iso 8601 to java date
	public void setDateOfBirth(String dateOfBirth) {
		DateTimeFormatter formatter = ISODateTimeFormat.dateOptionalTimeParser();
		this.dateOfBirth = formatter.parseDateTime(dateOfBirth).toDate();
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	//ssn
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	
	
}
