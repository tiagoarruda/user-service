package com.gamesys.test.userservice.customValidator;

import java.util.Arrays;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.LengthRule;
import org.passay.PasswordData;
import org.passay.PasswordValidator;


public class CustomPasswordValidator implements ConstraintValidator<Password, String> {

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		PasswordValidator validator  = new PasswordValidator(Arrays.asList(
		           new LengthRule(4, Integer.MAX_VALUE), 
		           new CharacterRule(EnglishCharacterData.UpperCase, 1),
		           new CharacterRule(EnglishCharacterData.LowerCase, 1),
		           new CharacterRule(EnglishCharacterData.Digit, 1)));
		
		PasswordData password = new PasswordData(value);
		
		return validator.validate(password).isValid();
	}

	
}
