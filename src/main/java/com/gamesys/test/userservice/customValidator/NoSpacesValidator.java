package com.gamesys.test.userservice.customValidator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NoSpacesValidator implements
ConstraintValidator<NoSpaces, String> {

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return !value.contains(" ");
	}
	
}
