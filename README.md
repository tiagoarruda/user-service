# User Service

This application allow you to create a user and password.
The user can only be created if it's present in a exclusion list.

## Create User
Create a new user.

### Validations

* username cannot be blank.
* username characters must be alphanumerical
* password must contains at least one upper case character
* password must contains at least one lower case character
* password must contains at least one digit
* password minimum size is four
* dateOfBirth must comply with ISO 8601 date pattern
* ssn is composed of 9 digits

### CURL

curl --request POST \
  --url http://localhost:8080/users \
  --header 'Content-Type: application/json' \
  --header 'Postman-Token: a13ae129-8526-49c0-a48f-38c7772f6d53' \
  --header 'cache-control: no-cache' \
  --data '{\n	"username": "Arruda",\n	"password": "aBc1234",\n	"dateOfBirth": "2019-01-01",\n	"ssn": "123456781"\n}'

### Response codes

* 201 - User created with success
* 400 - Bad request
* 401 - User currently present in exclusion list

## Get User

Get User by username.

### CURL

curl --request GET \
  --url http://localhost:8080/users/Arruda \
  --header 'Accept: application/json' \
  --header 'Content-Type: application/json' \
  --header 'Postman-Token: 4ffd3eae-c4eb-43fb-abee-a058992a5239' \
  --header 'cache-control: no-cache'

### Response codes

* 200 User found
* 400 Bad request
* 404 User not found

## Delete User

Delete user by username.

### CURL

curl --request DELETE \
  --url http://localhost:8080/users/Arruda \
  --header 'Postman-Token: f9eb05e2-cb72-4e89-9adc-3661b267a5ad' \
  --header 'cache-control: no-cache'

* 200 User deleted
* 400 Bad request
* User not found

## Create Exclusion

Create a new user exclusion based on dateOfBirth and SSN.

### CURL

curl --request POST \
  --url http://localhost:8080/exclusions \
  --header 'Content-Type: application/json' \
  --header 'Postman-Token: 9fe5a022-99c8-44ed-bf0a-f8818d6db213' \
  --header 'cache-control: no-cache' \
  --data '{\n	"dateOfBirth": "2018-01-01",\n	"ssn": "123456780"\n}'

### Response codes

* 201 Exclusion created
* 400 Bad request

## Delete Exclusion

Delete exclusion based on dateOfBirth and ssn.

### CURL

curl --request DELETE \
  --url 'http://localhost:8080/exclusions?dateOfBirth=2019-01-01&ssn=123456789' \
  --header 'Postman-Token: 88ff5f65-896a-4daf-bcb2-e72a4f7e5820' \
  --header 'cache-control: no-cache'

### Response codes

* 200 Exclusion deleted
* 404 Exclusion not found